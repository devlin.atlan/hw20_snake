// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"  
#include "Kismet/GameplayStatics.h"
#include <MySnake20\Snake20GameInstance.h>

// Sets default values
ASnakeBase::ASnakeBase()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 50.f;
	StepWidth = 20.f;
	Speed = 10.f;
	CurrentMoveDirection = EMoveDirection::RIGHT;
	PrevTickMoveDirection = EMoveDirection::LEFT;
		
		//	CurrentGameLevelIndex = FCString::Atoi(*CurrentGameLevelIndexName) - 1;
		
}
 

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(Speed);

//Если загрузился уровень, то беру его имя настраиваю меню.
	if (GetWorld())//UGameplayStatics::GetCurrentLevelName())
		{
			FString CurrentGameLevelIndexName = *GetWorld()->GetName();
			//int* LvlIndex = GetWorld()->GetGameInstance<USnake20GameInstance>()->LoadedLvlIndex;
		//	GameLevels.Find(CurrentGameLevelIndexName, GetWorld()->GetGameInstance<USnake20GameInstance>()->LoadedLvlIndex);
			if (CurrentGameLevelIndexName == "menu")
			{
				MovingCircle = true;
				FillPreparedElements();
			}
			else
//вторая переменная показывает, что у нас начало игры и не нужно создавать элементы скрытыми					
			{
		 		AddSnakeElements(1, true); 
			} 
		} 
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move(MovingCircle);
}

//isStart истинный при ASnakeBase::BeginPlay(), т.е. в начале игры, при создании змейки целиком. Иначе создаётся по одному элементу
void ASnakeBase::AddSnakeElements(int ElementsCnt, bool isStart)
{
	if (!MovingCircle)
	for (int I = 0; I < ElementsCnt; I++)
	{
		FVector NewLocation =  FVector().ZeroVector;
		if (SnakeElements.Num() == 0)
		{
			 NewLocation.Set(ElementSize, ((I % 2) - 0.5) * StepWidth + ElementSize, DefaultZLocation);
		}
		else
		{
			//NewLocation.Set(0,0, DefaultZLocation);
			//NewLocation.Set(SnakeElements[SnakeElements.Num() - 1]->GetActorLocation().X, SnakeElements[SnakeElements.Num() - 1]->GetActorLocation().Y, DefaultZLocation);
			 NewLocation.Set(ElementSize + SnakeElements[SnakeElements.Num() - 1]->GetActorLocation().X, ((I % 2) - 0.5) * StepWidth + ElementSize + SnakeElements[SnakeElements.Num() - 1]->GetActorLocation().Y, DefaultZLocation);
		}
		FTransform NewTransform(NewLocation + GetActorTransform().GetLocation());
		ASnakeElementBase* NewElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		//Делает актора невидимым при создании (потом возвращает видимость при передвижении), если только не начало игры
		if (!isStart) { NewElement->SetActorHiddenInGame(true); }
		NewElement->SnakeOwner = this;
		NewElement->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);
		int32 ElemIndex = SnakeElements.Add(NewElement);
		if (ElemIndex == 0)
		{
			NewElement->SetFirstElementMat();
			NewElement->MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
			NewElement->MeshComponent->SetCollisionResponseToChannel(ECC_WorldDynamic, ECR_Overlap);
			NewElement->MeshComponent->SetCollisionResponseToChannel(ECC_WorldStatic, ECR_Overlap);
		}
	}
}

void ASnakeBase::AddSnakeElements(FVector* NewLocation, int ElementsCnt /*= 1*/, bool isStart /*= false*/)
{
	for (int I = 0; I < ElementsCnt; I++)
	{  
		FTransform NewTransform(*NewLocation + GetActorTransform().GetLocation());
		ASnakeElementBase* NewElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
			
		NewElement->SnakeOwner = this;
		NewElement->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);

			UE_LOG(LogTemp, Warning, TEXT("Element placed at (%f,%f,%f)"), NewElement->GetActorLocation().X, NewElement->GetActorLocation().Y, NewElement->GetActorLocation().Z);

		int32 ElemIndex = SnakeElements.Add(NewElement);
		if (ElemIndex == 0)
		{
			NewElement->SetFirstElementMat();
			NewElement->MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
			NewElement->MeshComponent->SetCollisionResponseToChannel(ECC_WorldDynamic, ECR_Overlap);
			NewElement->MeshComponent->SetCollisionResponseToChannel(ECC_WorldStatic, ECR_Overlap);
		} 
		if (ElemIndex > 0 && MovingCircle) {
			NewElement->MeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
			NewElement->MeshComponent->SetCollisionResponseToChannel(ECC_WorldDynamic, ECR_Ignore);
			NewElement->MeshComponent->SetCollisionResponseToChannel(ECC_WorldStatic, ECR_Ignore);
		}
		//Делает актора невидимым при создании (потом возвращает видимость при передвижении), если только не начало игры
		if (!isStart) {
			NewElement->SetActorHiddenInGame(true);
		//	NewElement->MeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
			NewElement->MeshComponent->SetCollisionResponseToChannel(ECC_WorldDynamic, ECR_Ignore);
			NewElement->MeshComponent->SetCollisionResponseToChannel(ECC_WorldStatic, ECR_Ignore);
		}
		else
		{
		//	NewElement->MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
		}


	}
}

void ASnakeBase::Move(bool isCircleMove)
{
	//Счётчик меняется на чёт\нечёт
	TickEvenCounter = !TickEvenCounter;
	FVector LastElemLoc = FVector(0, 0, 0);
	FVector MoveVector = FVector(0, 0, 0);
	float MoveStepLength = ElementSize;

	switch (CurrentMoveDirection)
	{
	case EMoveDirection::UP:
		MoveVector.X += MoveStepLength;
		break;
	case EMoveDirection::DOWN:
		MoveVector.X -= MoveStepLength;
		break;
	case EMoveDirection::LEFT:
		MoveVector.Y -= MoveStepLength;
		break;
	case EMoveDirection::RIGHT:
		MoveVector.Y += MoveStepLength;
		break;
	}

		SnakeElements[0]->ToggleCollision(-1);//!!!!!!!!!!!!!!!!!!!! ВЫКЛЮЧЕНИЕ КОЛЛИЗИИ ДЛЯ ГОЛОВЫ  
	//Если змейка идёт по кругу, на всякий случай не меняю коллизию
	if (!isCircleMove)
	{
	}
	else
	{
		//Запоминается положение крайнего элемента
		LastElemLoc = SnakeElements[SnakeElements.Num() - 1]->GetActorLocation();
	}

	if (SnakeElements.Num() > 0)
	{
		for (int i = SnakeElements.Num() - 1; i > 0; i--)
		{
			auto CurrentElement = SnakeElements[i];
			auto PrevElement = SnakeElements[i - 1];
			FVector PrevElemLoc = PrevElement->GetActorLocation();


	//		FVector DeltaVector = (PrevElemLoc - CurrentElement->GetActorLocation());

			//!!!#### Если не ходим по кругу и вторая фигурка за головой, то пусть налево-направо извивается.
			if (i == 1 && !isCircleMove)
			{
				switch (CurrentMoveDirection)
				{
				case EMoveDirection::UP:
					PrevElemLoc.Y -= (((int)TickEvenCounter % 2) - 0.5) * StepWidth * i;
					break;
				case EMoveDirection::DOWN:
					PrevElemLoc.Y += (((int)TickEvenCounter % 2) - 0.5) * StepWidth * i;
					break;
				case EMoveDirection::LEFT:
					PrevElemLoc.X += (((int)TickEvenCounter % 2) - 0.5) * StepWidth * i;
					break;
				case EMoveDirection::RIGHT:
					PrevElemLoc.X -= (((int)TickEvenCounter % 2) - 0.5) * StepWidth * i;
					break;
				}
			}
			PrevElemLoc.Z = CurrentElement->GetActorLocation().Z;

			CurrentElement->SetActorLocation(PrevElemLoc);
			//Если перемещается крайний элемент, то он может быть новым и невидимым.
			if (i == SnakeElements.Num() - 1 && !isCircleMove)
			{
				CurrentElement->SetActorHiddenInGame(false);
			}
		}
		//Если змейка идёт по кругу, на всякий случай не меняю коллизию, а ещё ставлю первый элемент на место конечного  ----- убрал!!! Фикс коллизии типом объектов!
		if (!isCircleMove)
		{
			SnakeElements[0]->AddActorWorldOffset(MoveVector);
		}
		else
		{
			LastElemLoc.Z = DefaultZLocation;
			SnakeElements[0]->SetActorLocation(LastElemLoc);
		}
			SnakeElements[0]->ToggleCollision(1);//!!!!!!!!!!!!!!!!!!!! ВКЛЮЧЕНИЕ КОЛЛИЗИИ ДЛЯ ГОЛОВЫ
}
		PrevTickMoveDirection = CurrentMoveDirection;

}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* OtherActor)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractedInterface = Cast<IInteractable>(OtherActor);
		if (InteractedInterface)
		{
			InteractedInterface->Interact(this, bIsFirst, ElemIndex);
		//	InteractedInterface->Interact(OtherActor, bIsFirst);
		}
	}

}

//############ Функция смены уровня. Кэп.
void ASnakeBase::ChangeLevel(int Index /*= 0*/)
{
	if(GetWorld()->GetGameInstance<USnake20GameInstance>()->GameLevels.Num() ==0)
	{
		GetWorld()->GetGameInstance<USnake20GameInstance>()->FillLvlsArray();
	}
	GetWorld()->GetGameInstance<USnake20GameInstance>()->ChangeLevel();

	/*if ( Index  == 0) { Index++; }
	Index = GetWorld()->GetGameInstance<USnake20GameInstance>()->LoadedLvlIndex + Index;
	if (GetWorld()->GetGameInstance<USnake20GameInstance>()->GameLevels.Num()  > 
		(GetWorld()->GetGameInstance<USnake20GameInstance>()->LoadedLvlIndex + Index))
	{
		UGameplayStatics::OpenLevel(GetWorld(), StaticCast<FName>(GetWorld()->GetGameInstance<USnake20GameInstance>()->GameLevels[Index]));
	}*/
}

//############ Через интерфейс IHandPlaceble получаю инфу в лог, чем заполнить змейку. 
//Копирую оттуда пачку данных в эксель, сортирую, подставляю нужный текст, копирую всё... И заполняю в этой функции змейку. 
void ASnakeBase::FillPreparedElements()
{
	AddSnakeElements(new FVector(210.000000, 630.000000, 20.000000), 1, true); //LogTemp: Warning: 98!
	AddSnakeElements(new FVector(140.000000, 630.000000, 20.000000), 1, true); //LogTemp: Warning: 97!
	AddSnakeElements(new FVector(140.000000, 570.000000, 20.000000), 1, true); //LogTemp: Warning: 96!
	AddSnakeElements(new FVector(140.000000, 510.000000, 20.000000), 1, true); //LogTemp: Warning: 95!
	AddSnakeElements(new FVector(140.000000, 450.000000, 20.000000), 1, true); //LogTemp: Warning: 94!
	AddSnakeElements(new FVector(140.000000, 380.000000, 20.000000), 1, true); //LogTemp: Warning: 93!
	AddSnakeElements(new FVector(140.000000, 320.000000, 20.000000), 1, true); //LogTemp: Warning: 92!
	AddSnakeElements(new FVector(140.000000, 260.000000, 20.000000), 1, true); //LogTemp: Warning: 91!
	AddSnakeElements(new FVector(140.000000, 200.000000, 20.000000), 1, true); //LogTemp: Warning: 90!
	AddSnakeElements(new FVector(140.000000, 140.000000, 20.000000), 1, true); //LogTemp: Warning: 89!
	AddSnakeElements(new FVector(140.000000, 70.000000, 20.000000), 1, true); //LogTemp: Warning: 88!
	AddSnakeElements(new FVector(140.000000, 10.000000, 20.000000), 1, true); //LogTemp: Warning: 87!
	AddSnakeElements(new FVector(140.000000, -50.000000, 20.000000), 1, true); //LogTemp: Warning: 86!
	AddSnakeElements(new FVector(140.000000, -110.000000, 20.000000), 1, true); //LogTemp: Warning: 85!
	AddSnakeElements(new FVector(140.000000, -170.000000, 20.000000), 1, true); //LogTemp: Warning: 84!
	AddSnakeElements(new FVector(140.000000, -240.000000, 20.000000), 1, true); //LogTemp: Warning: 83!
	AddSnakeElements(new FVector(140.000000, -300.000000, 20.000000), 1, true); //LogTemp: Warning: 82!
	AddSnakeElements(new FVector(140.000000, -360.000000, 20.000000), 1, true); //LogTemp: Warning: 81!
	AddSnakeElements(new FVector(140.000000, -420.000000, 20.000000), 1, true); //LogTemp: Warning: 80!
	AddSnakeElements(new FVector(140.000000, -480.000000, 20.000000), 1, true); //LogTemp: Warning: 79!
	AddSnakeElements(new FVector(140.000000, -550.000000, 20.000000), 1, true); //LogTemp: Warning: 78!
	AddSnakeElements(new FVector(140.000000, -610.000000, 20.000000), 1, true); //LogTemp: Warning: 77!
	AddSnakeElements(new FVector(140.000000, -670.000000, 20.000000), 1, true); //LogTemp: Warning: 76!
	AddSnakeElements(new FVector(140.000000, -730.000000, 20.000000), 1, true); //LogTemp: Warning: 75!
	AddSnakeElements(new FVector(140.000000, -790.000000, 20.000000), 1, true); //LogTemp: Warning: 74!
	AddSnakeElements(new FVector(140.000000, -860.000000, 20.000000), 1, true); //LogTemp: Warning: 73!
	AddSnakeElements(new FVector(140.000000, -920.000000, 20.000000), 1, true); //LogTemp: Warning: 72!
	AddSnakeElements(new FVector(200.000000, -920.000000, 20.000000), 1, true); //LogTemp: Warning: 71!
	AddSnakeElements(new FVector(250.000000, -920.000000, 20.000000), 1, true); //LogTemp: Warning: 70!
	AddSnakeElements(new FVector(250.000000, -860.000000, 20.000000), 1, true); //LogTemp: Warning: 69!
	AddSnakeElements(new FVector(210.000000, -810.000000, 20.000000), 1, true); //LogTemp: Warning: 68!
	AddSnakeElements(new FVector(210.000000, -760.000000, 20.000000), 1, true); //LogTemp: Warning: 67!
	AddSnakeElements(new FVector(230.000000, -710.000000, 20.000000), 1, true); //LogTemp: Warning: 66!
	AddSnakeElements(new FVector(300.000000, -750.000000, 20.000000), 1, true); //LogTemp: Warning: 65!
	AddSnakeElements(new FVector(400.000000, -800.000000, 20.000000), 1, true); //LogTemp: Warning: 64!
	AddSnakeElements(new FVector(400.000000, -750.000000, 20.000000), 1, true); //LogTemp: Warning: 63!
	AddSnakeElements(new FVector(400.000000, -690.000000, 20.000000), 1, true); //LogTemp: Warning: 62!
	AddSnakeElements(new FVector(350.000000, -690.000000, 20.000000), 1, true); //LogTemp: Warning: 61!
	AddSnakeElements(new FVector(280.000000, -690.000000, 20.000000), 1, true); //LogTemp: Warning: 60!
	AddSnakeElements(new FVector(240.000000, -600.000000, 20.000000), 1, true); //LogTemp: Warning: 59!
	AddSnakeElements(new FVector(290.000000, -600.000000, 20.000000), 1, true); //LogTemp: Warning: 58!
	AddSnakeElements(new FVector(340.000000, -600.000000, 20.000000), 1, true); //LogTemp: Warning: 57!
	AddSnakeElements(new FVector(400.000000, -600.000000, 20.000000), 1, true); //LogTemp: Warning: 56!
	AddSnakeElements(new FVector(370.000000, -550.000000, 20.000000), 1, true); //LogTemp: Warning: 55!
	AddSnakeElements(new FVector(330.000000, -510.000000, 20.000000), 1, true); //LogTemp: Warning: 54!
	AddSnakeElements(new FVector(380.000000, -480.000000, 20.000000), 1, true); //LogTemp: Warning: 53!
	AddSnakeElements(new FVector(420.000000, -430.000000, 20.000000), 1, true); //LogTemp: Warning: 52!
	AddSnakeElements(new FVector(370.000000, -430.000000, 20.000000), 1, true); //LogTemp: Warning: 51!
	AddSnakeElements(new FVector(320.000000, -430.000000, 20.000000), 1, true); //LogTemp: Warning: 50!
	AddSnakeElements(new FVector(260.000000, -430.000000, 20.000000), 1, true); //LogTemp: Warning: 49!
	AddSnakeElements(new FVector(200.000000, -430.000000, 20.000000), 1, true); //LogTemp: Warning: 48!
	AddSnakeElements(new FVector(220.000000, -280.000000, 20.000000), 1, true); //LogTemp: Warning: 471!
	AddSnakeElements(new FVector(220.000000, -230.000000, 20.000000), 1, true); //LogTemp: Warning: 470!
	AddSnakeElements(new FVector(220.000000, -180.000000, 20.000000), 1, true); //LogTemp: Warning: 46!
	AddSnakeElements(new FVector(220.000000, -230.000000, 20.000000), 1, true); //LogTemp: Warning: 451!
	AddSnakeElements(new FVector(220.000000, -280.000000, 20.000000), 1, true); //LogTemp: Warning: 450!
	AddSnakeElements(new FVector(220.000000, -330.000000, 20.000000), 1, true); //LogTemp: Warning: 44!
	AddSnakeElements(new FVector(270.000000, -330.000000, 20.000000), 1, true); //LogTemp: Warning: 43!
	AddSnakeElements(new FVector(320.000000, -330.000000, 20.000000), 1, true); //LogTemp: Warning: 42!
	AddSnakeElements(new FVector(320.000000, -280.000000, 20.000000), 1, true); //LogTemp: Warning: 411!
	AddSnakeElements(new FVector(320.000000, -230.000000, 20.000000), 1, true); //LogTemp: Warning: 410!
	AddSnakeElements(new FVector(320.000000, -180.000000, 20.000000), 1, true); //LogTemp: Warning: 40!
	AddSnakeElements(new FVector(320.000000, -230.000000, 20.000000), 1, true); //LogTemp: Warning: 391!
	AddSnakeElements(new FVector(320.000000, -280.000000, 20.000000), 1, true); //LogTemp: Warning: 390!
	AddSnakeElements(new FVector(370.000000, -330.000000, 20.000000), 1, true); //LogTemp: Warning: 38!
	AddSnakeElements(new FVector(420.000000, -330.000000, 20.000000), 1, true); //LogTemp: Warning: 37!
	AddSnakeElements(new FVector(420.000000, -280.000000, 20.000000), 1, true); //LogTemp: Warning: 36!
	AddSnakeElements(new FVector(420.000000, -230.000000, 20.000000), 1, true); //LogTemp: Warning: 35!
	AddSnakeElements(new FVector(450.000000, -180.000000, 20.000000), 1, true); //LogTemp: Warning: 34!
	AddSnakeElements(new FVector(420.000000, -90.000000, 20.000000), 1, true); //LogTemp: Warning: 33!
	AddSnakeElements(new FVector(350.000000, -90.000000, 20.000000), 1, true); //LogTemp: Warning: 32!
	AddSnakeElements(new FVector(290.000000, -90.000000, 20.000000), 1, true); //LogTemp: Warning: 31!
	AddSnakeElements(new FVector(220.000000, -90.000000, 20.000000), 1, true); //LogTemp: Warning: 30!
	AddSnakeElements(new FVector(270.000000, -40.000000, 20.000000), 1, true); //LogTemp: Warning: 29!
	AddSnakeElements(new FVector(320.000000, -10.000000, 20.000000), 1, true); //LogTemp: Warning: 28!
	AddSnakeElements(new FVector(370.000000, 20.000000, 20.000000), 1, true); //LogTemp: Warning: 27!
	AddSnakeElements(new FVector(420.000000, 70.000000, 20.000000), 1, true); //LogTemp: Warning: 26!
	AddSnakeElements(new FVector(360.000000, 70.000000, 20.000000), 1, true); //LogTemp: Warning: 25!
	AddSnakeElements(new FVector(290.000000, 70.000000, 20.000000), 1, true); //LogTemp: Warning: 24!
	AddSnakeElements(new FVector(220.000000, 70.000000, 20.000000), 1, true); //LogTemp: Warning: 23!
	AddSnakeElements(new FVector(220.000000, 150.000000, 20.000000), 1, true); //LogTemp: Warning: 22!
	AddSnakeElements(new FVector(270.000000, 150.000000, 20.000000), 1, true); //LogTemp: Warning: 21!
	AddSnakeElements(new FVector(320.000000, 150.000000, 20.000000), 1, true); //LogTemp: Warning: 20!
	AddSnakeElements(new FVector(380.000000, 150.000000, 20.000000), 1, true); //LogTemp: Warning: 19!
	AddSnakeElements(new FVector(430.000000, 150.000000, 20.000000), 1, true); //LogTemp: Warning: 18!
	AddSnakeElements(new FVector(350.000000, 200.000000, 20.000000), 1, true); //LogTemp: Warning: 17!
	AddSnakeElements(new FVector(390.000000, 230.000000, 20.000000), 1, true); //LogTemp: Warning: 16!
	AddSnakeElements(new FVector(430.000000, 290.000000, 20.000000), 1, true); //LogTemp: Warning: 15!
	AddSnakeElements(new FVector(390.000000, 230.000000, 20.000000), 1, true); //LogTemp: Warning: 14!
	AddSnakeElements(new FVector(310.000000, 240.000000, 20.000000), 1, true); //LogTemp: Warning: 13!
	AddSnakeElements(new FVector(250.000000, 250.000000, 20.000000), 1, true); //LogTemp: Warning: 12!
	AddSnakeElements(new FVector(220.000000, 310.000000, 20.000000), 1, true); //LogTemp: Warning: 11!
	AddSnakeElements(new FVector(220.000000, 370.000000, 20.000000), 1, true); //LogTemp: Warning: 10!
	AddSnakeElements(new FVector(270.000000, 390.000000, 20.000000), 1, true); //LogTemp: Warning: 09!
	AddSnakeElements(new FVector(310.000000, 470.000000, 20.000000), 1, true); //LogTemp: Warning: 08!
	AddSnakeElements(new FVector(330.000000, 410.000000, 20.000000), 1, true); //LogTemp: Warning: 07!
	AddSnakeElements(new FVector(380.000000, 430.000000, 20.000000), 1, true); //LogTemp: Warning: 06!
	AddSnakeElements(new FVector(420.000000, 480.000000, 20.000000), 1, true); //LogTemp: Warning: 05!
	AddSnakeElements(new FVector(370.000000, 520.000000, 20.000000), 1, true); //LogTemp: Warning: 04!
	AddSnakeElements(new FVector(320.000000, 530.000000, 20.000000), 1, false); //LogTemp: Warning: 03!
	AddSnakeElements(new FVector(270.000000, 540.000000, 20.000000), 1, false); //LogTemp: Warning: 02!
	AddSnakeElements(new FVector(210.000000, 570.000000, 20.000000), 1, false); //LogTemp: Warning: 01!
}

