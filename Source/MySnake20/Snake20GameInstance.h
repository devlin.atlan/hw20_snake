// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Snake20GameInstance.generated.h"
 
/**
 * 
 */
UCLASS()
class MYSNAKE20_API USnake20GameInstance : public UGameInstance
{
	GENERATED_BODY()

public: 

	UPROPERTY(EditAnywhere)
	bool IsGameEnded;
	UPROPERTY(EditAnywhere)
	TArray<FString> GameLevels;
	UPROPERTY(EditAnywhere)
		int LoadedLvlIndex;
	UFUNCTION() 
	void FillLvlsArray();
	UFUNCTION()
		TArray<FString> GetLvlsArray();
	UFUNCTION() 
		void ChangeLevel(FString LvlName = "_none_"); 
	UFUNCTION() 
        void ResetLevel(); 
};
