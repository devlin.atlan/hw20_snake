// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Interactable.h"
#include "GameFramework/Actor.h" 
#include "Walls.generated.h"

class ALevelPickItem;
UCLASS()
class MYSNAKE20_API AWalls : public AActor, public IInteractable
{
	GENERATED_BODY()


public:	
	// Sets default values for this actor's properties
	AWalls();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	UPROPERTY(EditAnywhere)
	TSubclassOf<ALevelPickItem> LevelPickClass;
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void Interact(AActor* Target, bool bIsHeadElement, int AdditionalIndex=-1) override;

};
