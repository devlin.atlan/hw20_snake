// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeElementBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include <MySnake20\Snake20GameInstance.h>
#include "SnakeBase.h"  
#include "HandPlaceble.h"
#include "Kismet/GameplayStatics.h"
#include "LevelPickItem.h"

 

// Sets default values
ASnakeElementBase::ASnakeElementBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true ;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
//	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
//	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
	MeshComponent->AddLocalRotation(FQuat(90, 0, 90, 0));
	//	 MeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	 	// MeshComponent->SetCollisionResponseToChannel(ECC_WorldDynamic, ECR_Ignore);
		MeshComponent->SetCollisionResponseToChannel(ECC_WorldStatic, ECR_Ignore);
	
//	MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ASnakeElementBase::HandleBeginOverlap);
//	OutputAdditionalInfo = "Moo: ";
}

// Called when the game starts or when spawned
void ASnakeElementBase::BeginPlay()
{
	Super::BeginPlay();
	LogCoords();
//### Задаю тут нужные параметры коллизий 
}

// Called every frame
void ASnakeElementBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}
 
void ASnakeElementBase::Interact(AActor* Target, bool bIsHeadElement, int AdditionalIndex)
{
	auto Snake = Cast<ASnakeBase>(Target);
	if (IsValid(Snake))
	{
	/*	FVector NewLocation(50,50,110);
		FTransform NewTransform( NewLocation + GetActorTransform().GetLocation());
//!!!! НЕ ПАШЕТ		ALevelPickItem* NewElement = GetWorld()->SpawnActor<ALevelPickItem>(LevelPickClass, NewTransform);
	//	NewElement->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);
		Snake->Destroy();*/
		GetWorld()->GetGameInstance<USnake20GameInstance>()->ResetLevel();
		this->Destroy();
	}
}

void ASnakeElementBase::LogCoords()
{ 
	if (GetWorld())
	{
		TArray<AActor*> HandPlacebles;
		int CurIndex=0;
	 
		UE_LOG(LogTemp, Warning, TEXT("%s!(%f,%f,%f)"), *OutputText, this->GetActorLocation().X, this->GetActorLocation().Y, this->GetActorLocation().Z); 
	} 
 
/*!!!! Шпаргалка по выводу в консоль  и на экран
	UE_LOG(LogTemp, Warning, TEXT("Your message"));
	UE_LOG(YourLog, Warning, TEXT("This is a message to yourself during runtime!"));

	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("This is an on screen message!"));
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("Some variable values: x: %f, y: %f"), x, y));
*/
}

void ASnakeElementBase::SetFirstElementMat_Implementation()
{
	MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ASnakeElementBase::HandleBeginOverlap);
}
/*void ASnakeElementBase::SetFirstElementMat()//_Implementation()
{
	MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ASnakeElementBase::HandleBeginOverlap);
}*/

void ASnakeElementBase::HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor, 
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult &SweepResult)
{
	if (IsValid(SnakeOwner))
	{
		SnakeOwner->SnakeElementOverlap(this, OtherActor);
		UE_LOG(LogTemp, Warning, TEXT("Element overlapped"));
	}
} 
void ASnakeElementBase::ToggleCollision(int CollisionEnable)
{
//!!!!!!!!!#### Теперь часть нужных параметров коллизии задаются в Бегине.
	if ((MeshComponent->GetCollisionResponseToChannel(ECC_WorldStatic) != ECR_Ignore && CollisionEnable == 0 )|| CollisionEnable == -1)
	{
	//	MeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		// !!!!!!!##########  РАБОЧИЕ ФУНКЦИИ
	//	MeshComponent->SetCollisionResponseToChannel(ECC_WorldDynamic, ECR_Overlap);
		MeshComponent->SetCollisionResponseToChannel(ECC_WorldStatic, ECR_Ignore);
	}
	else
	{
		//MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
		MeshComponent->SetCollisionResponseToChannel(ECC_WorldStatic, ECR_Overlap);
	}
}

