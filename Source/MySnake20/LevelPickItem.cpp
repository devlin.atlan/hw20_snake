// Fill out your copyright notice in the Description page of Project Settings.


#include "LevelPickItem.h" 
#include "SnakeBase.h"
#include <Kismet/GameplayStatics.h>  
#include "Engine/CollisionProfile.h"
#include <MySnake20\Snake20GameInstance.h>
#include <Components/TextRenderComponent.h> 

// Sets default values
ALevelPickItem::ALevelPickItem()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ALevelPickItem::BeginPlay()
{
	Super::BeginPlay(); 
	if (GetWorld())
	{ 
		IsPickedByMouse = false;
		IsPickedByTouch = false;
		OnClicked.AddUniqueDynamic(this, &ALevelPickItem::ClickTake);
		//UStaticMeshComponent* MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	//	APlayerController* PC = GetWorld()->GetFirstPlayerController();
		//PC->InputComponent->BindAction(EInputEvent::IE_Pressed, this, &ALevelPickItem::TouchTake);
		OnInputTouchBegin.AddDynamic(this, &ALevelPickItem::TouchTake);
		OnInputTouchEnd.AddDynamic(this, &ALevelPickItem::TouchLeft);
		OnReleased.AddUniqueDynamic(this, &ALevelPickItem::ClickLeft);
		this->SetActorTickEnabled(true);
		
	//	��� ������ ����� ��� ��������
		UTextRenderComponent* TR = Cast<UTextRenderComponent>(GetComponentByClass(UTextRenderComponent::StaticClass()));
		if (TR) 
		{ 
			TR->SetText((TextTitle)) ;
		}
	//	��� ��������� ��������� �������� ��� �������� �� ������ �������.
	/*	auto* MC = GetComponentByClass(UMeshComponent::StaticClass());
		if (MC)
		{
			dynamic_cast<UMeshComponent*>(TR)->OnComponentBeginOverlap.AddDynamic(this, &ALevelPickItem::ChangeLevel);
		} */
	}
}

// Called every frame
void ALevelPickItem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
 	if (IsPickedByMouse || IsPickedByTouch)
	{
		MoveToPointer();
	} 
}


 //##### ���������� ��� ����� ����� ������� ����� �� ������
void ALevelPickItem::ClickTake(AActor* Target, FKey ButtonPressed)
{
	if (ButtonPressed.GetFName() ==  "LeftMouseButton")
	{
		UE_LOG(LogTemp, Warning, TEXT("Event: Picked!"));
		IsPickedByMouse = true;
		TakeActor(true,false);
	}
}

void ALevelPickItem::TouchTake(ETouchIndex::Type FingerIndex, AActor* TouchedActor)
{
//	if (FingerIndex>=0)
//	{
	TouchKey = FingerIndex;
	IsPickedByTouch = true;
		UE_LOG(LogTemp, Warning, TEXT("Event: Touch-Picked!"));
		TakeActor(false,true);
//	}
}

//##### ���������� ��� ���������� ����� ������ ����� �� ������
void ALevelPickItem::ClickLeft(AActor* Target, FKey ButtonPressed)
{
	if (ButtonPressed.GetFName() == "LeftMouseButton")
	{
		UE_LOG(LogTemp, Warning, TEXT("Event: Dropped!"));
		DropActor();
	}
}
void ALevelPickItem::TouchLeft(ETouchIndex::Type FingerIndex, AActor* TouchedActor)
{
///	if (FingerIndex >= 0)
//	{
	// FingerIndex;
		UE_LOG(LogTemp, Warning, TEXT("Event: Touch-Dropped!"));
		DropActor();
	//}
}
void ALevelPickItem::TakeActor(bool TakenByMouse, bool TakenByTouch)
{
//	UE_LOG(LogTemp, Warning, TEXT("Event: Picked!"));
	if (TakenByMouse)
	{
		//		DefaultZLocation = GetActorLocation().Z;
		IsPickedByMouse = true;
	}
	if (TakenByTouch)
	{
		//		DefaultZLocation = GetActorLocation().Z;
		IsPickedByTouch = true;
	}

		
}



void ALevelPickItem::DropActor()
{
//	UE_LOG(LogTemp, Warning, TEXT("Event: Dropped!"));
	FVector NormZLocation;
	NormZLocation = GetActorLocation();
	NormZLocation.Z = DropZLocation;
	this->SetActorLocation(NormZLocation, false, nullptr, ETeleportType::TeleportPhysics);
	IsPickedByMouse = false;
	IsPickedByTouch = false;
}

//##### ���������� ������ ��� �� ���������� ����� ������ ����� �� ������
void ALevelPickItem::MoveToPointer()
{  
	//########!!!!!!!!!! ���������� ��������� ������ ������� (FVector?) ���� �������� �� ������� �� "����" � ���������� �����
	UE_LOG(LogTemp, Warning, TEXT("Event: Dragging!"));
	
	 
	
	APlayerController* PC = GetWorld()->GetFirstPlayerController();
	
	//��������, ������ �� �� ��� ��� ��� ���\����, ������� ������� ������. ���� ���, �� �������.
	float tX, tY;
	bool touchPressed;
	PC->GetInputTouchState(TouchKey, tX, tY, touchPressed);
	if ((IsPickedByMouse && !PC->IsInputKeyDown("LeftMouseButton")) || (IsPickedByTouch && !touchPressed)) 
	{
		DropActor();
		UE_LOG(LogTemp, Warning, TEXT("Event: Dragging --- lost!")); 
		return;
	}
	//APawn*a = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);
	//APlayerPawnBase* PP = Cast<APlayerPawnBase>(AC);
	//APlayerPawnBase* PP = Cast<APlayerPawnBase>();// ActorsOfClass(GetWorld(), APlayerPawnBase::StaticClass(), PlayerPawnBases);
		// s(GetPlayerCharacter(GetWorld(), 0));
 //	APlayerController* PC = StaticCast<APlayerPawnBase>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0)).GetPC();
	//APlayerController* PC = (Cast<APlayerPawnBase>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0)))->GetPC();
	//UGameplayStatics::GetPlayerCharacter(GetWorld(), 0)->GetController();

	if (PC)
	{

		// ��� ������ ��� �� ��������� � �������� ��������.
		float locX, locY, locZ;
		FVector a, b;

		TArray<TEnumAsByte<EObjectTypeQuery>> objTypes;
		 
		ECollisionChannel CollisionChannel;
		FCollisionResponseParams ResponseParams;  
		if (UCollisionProfile::GetChannelAndResponseParams(FName(TEXT("BGObjectProfile")), CollisionChannel, ResponseParams)) 
		{
			objTypes.Add(UEngineTypes::ConvertToObjectType(CollisionChannel));
		}
		 
		FHitResult HR;
		PC->GetHitResultUnderCursorForObjects(objTypes,true, HR); 

		DropZLocation = HR.Location.Z+ DefaultZLocation;
			locX = HR.Location.X; locY = HR.Location.Y; locZ = DropZLocation +PickedZLocation;
 /*	PC->DeprojectMousePositionToWorld(a, b);// GetMousePosition(locX, locY);
		//PC->GetMousePosition(locX, locY);
		locX = a.X; locY = a.Y;
		UE_LOG(LogTemp, Warning, TEXT("X: %f, Y: %f"), locX, locY);
		locX = b.X; locY = b.Y;
*/	
			


//�����������!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
/*PC->GetMousePosition(locX, locY); 
UE_LOG(LogTemp, Warning, TEXT("Mouse X: %f, Y: %f, Z: %f"), locX, locY, locZ);
locX = this->GetActorLocation().X;
locY = this->GetActorLocation().Y;
UE_LOG(LogTemp, Warning, TEXT("Actor X: %f, Y: %f, Z: %f"), locX, locY, locZ);
PC->GetMousePosition(locX, locY);
locX /= this->GetActorLocation().X;
locY /= this->GetActorLocation().Y;
UE_LOG(LogTemp, Warning, TEXT("Multi X: %f, Y: %f, Z: %f"), locX, locY, locZ);
FVector NewLocation = HR.Location;
NewLocation.Z = this->GetActorLocation().Z;
this->SetActorLocation(NewLocation, false, nullptr, ETeleportType::TeleportPhysics);
*/



//// ��� ��������!
		FVector NewLocation= HR.Location;
		NewLocation.Z = locZ;//this->GetActorLocation().Z;
		this->SetActorLocation(NewLocation, false, nullptr, ETeleportType::TeleportPhysics);
	
	}
}
void ALevelPickItem::Interact(AActor* Target, bool bIsHeadElement, int AdditionalIndex)
{
///########### ������ ������� ������ ���� ����� �������!
	if (bIsHeadElement && !IsPickedByMouse && !IsPickedByTouch)
	{
		auto* Snake = Cast<ASnakeBase>(Target);
		if (IsValid(Snake))
		{ 
			ChangeLevel(); 
		}
	}
}
 

//!!!!!!!!!!!!!####################################!!! ����������� ������� � GameInstance
void ALevelPickItem::ChangeLevel( )
{ 
	GetWorld()->GetGameInstance<USnake20GameInstance>()->ChangeLevel(LvlName);
		this->Destroy();
	/*if (GetWorld())//UGameplayStatics::GetCurrentLevelName())
	{   
		//ULevel* LastLvl = GetWorld()->GetCurrentLevel();
		
			UGameplayStatics::OpenLevel(GetWorld(), StaticCast<FName>(LvlName));
			
		

	}*/
	
}




