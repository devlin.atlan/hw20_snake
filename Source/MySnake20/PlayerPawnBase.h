// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PlayerPawnBase.generated.h"


class UCameraComponent;
class ASnakeBase;


UCLASS()
class MYSNAKE20_API APlayerPawnBase : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPawnBase();
	UPROPERTY(BlueprintReadWrite)
		UCameraComponent* PawnCamera;
	
	UPROPERTY(BlueprintReadWrite)
		ASnakeBase* SnakeActor;

	UPROPERTY(EditAnywhere)
		bool SetMouseEnabled;

	UPROPERTY(EditAnywhere)
	bool MyCameraPosDebug = false;
	UPROPERTY(EditAnywhere)
		float CameraMoveLimit=800.f;
	UPROPERTY(EditAnywhere)
		float CameraMoveStep=550.f;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeBase> SnakeActorClass;
	UPROPERTY()
		APlayerController* PC;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void CreateSnakeActor();
	void printLoc(float X, float Y);
	UFUNCTION()
		APlayerController* GetPC();
	UFUNCTION()
		void HandlePawnGetVAxis(float value);
	UFUNCTION()
		void HandlePawnGetHAxis(float value);
};
