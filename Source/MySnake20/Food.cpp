// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include "Kismet/GameplayStatics.h" 
#include <MySnake20\Snake20GameInstance.h>
  

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

} 

void AFood::Interact(AActor* Target, bool bIsHeadElement, int AdditionalIndex)
{
	if (bIsHeadElement)
	{
		auto* Snake = Cast<ASnakeBase>(Target);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElements();
//�������� ������ ���� FOODS �� �����. ���� ���� ��������� - ������� �������.
			TArray<AActor*> Foods;
			UGameplayStatics::GetAllActorsOfClass(GetWorld(), AFood::StaticClass(), Foods);
			if (Foods.Num() == 1)
			{
				GetWorld()->GetGameInstance<USnake20GameInstance>()->ChangeLevel();
			}
			this->Destroy();
		}
	}
}

