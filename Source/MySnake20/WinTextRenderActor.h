// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/TextRenderActor.h"
#include "WinTextRenderActor.generated.h"

/**
 * 
 */
UCLASS()
class MYSNAKE20_API AWinTextRenderActor : public ATextRenderActor
{
	GENERATED_BODY()
	AWinTextRenderActor();
	protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
};
