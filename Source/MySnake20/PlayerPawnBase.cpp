 // Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "SnakeBase.h" 
#include "SnakeElementBase.h"
#include "Components/InputComponent.h" 

#include "Kismet/GameplayStatics.h"

// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
//	PawnCamera->ProjectionMode= ECameraProjectionMode::Orthographic;
	RootComponent = PawnCamera;
	//RootComponent->AddLocalOffset(FVector (0, 0, -500));
	CameraMoveLimit *= 0.7;
	CameraMoveStep *= 0.8;
}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90, 0, 0));
	CreateSnakeActor();
	if(GEngine)
	{
		GEngine->SetMaxFPS(30);
	}
	
	//###### ��� �������� �����!!!!!!!!!!!!!!!!!
	PC = Cast<APlayerController>(GetController());

	if (PC)
	{
		PC->bShowMouseCursor = true;
		PC->bEnableClickEvents = true;
		PC->bEnableMouseOverEvents = true;
		PC->bEnableTouchEvents = true;
		PC->bEnableTouchOverEvents = true;
		//PC->ActivateTouchInterface()
	}
	/*float locX, locY;
	PC->GetMousePosition(locX, locY);
	printLoc(locX,locY);
	//TArray<AActor*> ArrDragable;
	//UGameplayStatics::GetAllActorsWithInterface(GetWorld(), IDragable::UClassType, ArrDragable);
	//TArray < TEnumAsByte<EObjectTypeQuery>>* arr = new TArray < TEnumAsByte<EObjectTypeQuery>>(); 
	//arr->Add(EObjectTypeQuery (IDragable) }); 
	FHitResult HR; 
	PC->GetHitResultUnderCursor(ECC_OverlapAll_Deprecated ,true, HR);
	printLoc(HR.Location.X, HR.Location.Y);
//	PC->OnClicked.Add( printLoc(locX,locY));
	 */
}
 

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	//FVector CameraLocation;
	// etSocketWorldLocationAndRotation(new FName, CameraLocation, new FQuat);
	//FVector CamSnakeDeltaLocation = GetActorLocation() - SnakeActor->SnakeElements[0].GetActorLocation(); 
	if (IsValid(SnakeActor) && IsValid(SnakeActor->SnakeElements[0]) && !SnakeActor->MovingCircle)
	{
	//	FVector a = SnakeActor->SnakeElements[0]->GetActorLocation();
		FVector CamSnakeDeltaLocation = PawnCamera->K2_GetComponentLocation() - SnakeActor->SnakeElements[0]->GetActorLocation();
	
		if (MyCameraPosDebug) {
			printLoc(CamSnakeDeltaLocation.X, CamSnakeDeltaLocation.Y);
		}
		//FVector CamSnakeDeltaLocation = PawnCamera->K2_GetComponentLocation()  - SnakeActor->GetActorLocation();
		if (CamSnakeDeltaLocation.X > CameraMoveLimit * 0.7)
		{
			//SetActorLocation()
			PawnCamera->AddWorldOffset(FVector{ -CameraMoveStep * 0.45f,0,0 }, false, nullptr, ETeleportType::None);
		}
		if (CamSnakeDeltaLocation.X < -CameraMoveLimit * 0.7)
		{
			PawnCamera->AddWorldOffset(FVector{ CameraMoveStep * 0.45f,0,0 }, false, nullptr, ETeleportType::None);
		}
		if (CamSnakeDeltaLocation.Y > CameraMoveLimit)
		{
			PawnCamera->AddWorldOffset(FVector{ 0,-CameraMoveStep,0 }, false, nullptr, ETeleportType::None);
		}
		if (CamSnakeDeltaLocation.Y < -CameraMoveLimit )
		{
			PawnCamera->AddWorldOffset(FVector{0,CameraMoveStep ,0 }, false, nullptr, ETeleportType::None);
		}
	}
}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandlePawnGetVAxis);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlePawnGetHAxis);

}

 void APlayerPawnBase::CreateSnakeActor()
 {
	 SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform(FVector(0,0,0)));
 }

 void APlayerPawnBase::printLoc(float X, float Y)
 {
	 UE_LOG(LogTemp, Warning, TEXT("X: %f, Y: %f"), X, Y); 
 }

 APlayerController* APlayerPawnBase::GetPC()
 {
	 return PC;
 }

 void APlayerPawnBase::HandlePawnGetVAxis(float value)
 {
	 if (IsValid(SnakeActor))
	 {
		 if (value > 0 && SnakeActor->PrevTickMoveDirection!=EMoveDirection::DOWN)
		 {
			 SnakeActor->CurrentMoveDirection = EMoveDirection::UP;
		 }
		 else if (value < 0 && SnakeActor->PrevTickMoveDirection != EMoveDirection::UP)
		 {
			 SnakeActor->CurrentMoveDirection = EMoveDirection::DOWN;
		 }
	 }
 }
 void APlayerPawnBase::HandlePawnGetHAxis(float value)
 {
	 if (IsValid(SnakeActor))
	 {
		 if (value > 0 && SnakeActor->PrevTickMoveDirection != EMoveDirection::RIGHT)
		 {
			 SnakeActor->CurrentMoveDirection = EMoveDirection::LEFT;
		 }
		 else if (value < 0 && SnakeActor->PrevTickMoveDirection != EMoveDirection::LEFT)
		 {
			 SnakeActor->CurrentMoveDirection = EMoveDirection::RIGHT;
		 }
	 }
 }

