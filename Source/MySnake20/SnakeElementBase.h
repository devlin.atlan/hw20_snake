// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "HandPlaceble.h"
#include "SnakeElementBase.generated.h"

class UStaticMeshComponent;
class ASnakeBase; 
class ALevelPickItem;

UCLASS()
class MYSNAKE20_API ASnakeElementBase : public AActor, public IInteractable, public IHandPlaceble
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeElementBase();

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
	UStaticMeshComponent* MeshComponent;

	UPROPERTY()
	ASnakeBase* SnakeOwner;

	UPROPERTY(EditAnywhere)
		FString OutputText;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ALevelPickItem> LevelPickClass;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	 void Interact(AActor* Target, bool bIsHeadElement, int AdditionalIndex) override;
	 void LogCoords() override;
	  

	UFUNCTION(BlueprintNativeEvent)
	void SetFirstElementMat(); 
	void SetFirstElementMat_Implementation();

	UFUNCTION()
		void HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent,
			AActor* OtherActor,
			UPrimitiveComponent* OtherComp,
			int32 OtherBodyIndex,
			bool bFromSweep,
			const FHitResult &SweepResult);
	void ToggleCollision(int CollisionEnable = 0 /*0 = �� �������, 1 = ��������, -1 = ��������� */);
};
