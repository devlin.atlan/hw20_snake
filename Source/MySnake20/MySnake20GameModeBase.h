// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MySnake20GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class MYSNAKE20_API AMySnake20GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
