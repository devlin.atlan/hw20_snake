// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Dragable.h"
#include "Interactable.h"
#include "LevelPickItem.generated.h"

class APlayerPawnBase;  

UCLASS()
class MYSNAKE20_API ALevelPickItem : public AActor, public IDragable, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ALevelPickItem();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	
	UPROPERTY()
		bool IsPickedByMouse;
	UPROPERTY()
		bool IsPickedByTouch;
	ETouchIndex::Type  TouchKey;
	float DropZLocation;
	float DefaultZLocation;
	UPROPERTY(EditAnywhere)
	float  PickedZLocation = 30.f;
	UPROPERTY(EditAnywhere)
		FString TextTitle;
	UPROPERTY(EditAnywhere)
		FString LvlName;
//	APlayerController TargetPointer;
//	FVector* LastPointerLocation;
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	UFUNCTION()
		void ClickTake(AActor* Target, FKey ButtonPressed);
	UFUNCTION()
		void TouchTake(ETouchIndex::Type FingerIndex, AActor* TouchedActor);
	
	UFUNCTION()
		void ClickLeft(AActor* Target, FKey ButtonPressed);
	UFUNCTION()
		void TouchLeft(ETouchIndex::Type FingerIndex, AActor* TouchedActor);
	void TakeActor(bool TakenByMouse, bool TakenByTouch);
	void DropActor();

	void MoveToPointer(); 
	void ChangeLevel();
	void Interact(AActor* Target, bool bIsHeadElement, int AdditionalIndex) override; 
};
