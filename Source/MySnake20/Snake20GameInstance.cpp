// Fill out your copyright notice in the Description page of Project Settings.


#include "Snake20GameInstance.h"
#include "Kismet/GameplayStatics.h"
 

//################################################################################ 
//Заполняю массив с названиями уровней чтобы их менять. 
 

void USnake20GameInstance::FillLvlsArray()
{ 
		GameLevels.Add("menu");
		GameLevels.Add("lvl1");
		GameLevels.Add("lvl2");  
		GameLevels.Add("lvl3");
		GameLevels.Add("lvl4");
		GameLevels.Add("lvl5");
		GameLevels.Add("lvl6");
		LoadedLvlIndex = -1;
}

TArray<FString> USnake20GameInstance::GetLvlsArray()
{ 
	return GameLevels; 
}
 

void USnake20GameInstance::ChangeLevel(FString LvlName /*= "_none_"*/)
{
	if (GameLevels.Num() == 0)
	{
		FillLvlsArray();
	}
	if (GetWorld())//UGameplayStatics::GetCurrentLevelName())
	{
		//	ULevel* LastLvl = GetWorld()->GetCurrentLevel();
//Убираем значение переменной, которая показывает, что игра пройдена.
		IsGameEnded = false;
		if (LvlName == "_none_")
		{
			if (LoadedLvlIndex == GameLevels.Num() - 1)
			{
//Возвращаемся в меню и активируем переменную для отображения победной анимации!
				LoadedLvlIndex = -1;
				IsGameEnded = true;
			}
			LoadedLvlIndex++;
		}
		else
		{
			GameLevels.Find(LvlName, LoadedLvlIndex);
		}
		
		UGameplayStatics::OpenLevel(GetWorld(), StaticCast<FName>(GameLevels[LoadedLvlIndex]));
		//LastLvl->destroyBeginDestroy();
	//	LastLvl->FinishDestroy(); 
	}
}

void USnake20GameInstance::ResetLevel()
{
	ChangeLevel(UGameplayStatics::GetCurrentLevelName(GetWorld()));
}
 