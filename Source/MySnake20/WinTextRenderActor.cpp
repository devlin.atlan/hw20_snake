// Fill out your copyright notice in the Description page of Project Settings.


#include "WinTextRenderActor.h"
#include "Snake20GameInstance.h"

AWinTextRenderActor::AWinTextRenderActor()
{
}

void AWinTextRenderActor::BeginPlay()
{
    Super::BeginPlay();
    SetActorHiddenInGame(!GetWorld()->GetGameInstance<USnake20GameInstance>()->IsGameEnded);
}
