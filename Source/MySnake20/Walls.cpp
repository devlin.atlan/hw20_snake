// Fill out your copyright notice in the Description page of Project Settings.


#include "Walls.h"
#include "LevelPickItem.h"
#include "SnakeBase.h"  
#include <MySnake20\Snake20GameInstance.h>

// Sets default values
AWalls::AWalls()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AWalls::BeginPlay()
{
	Super::BeginPlay(); 
	//LevelPickClass = ALevelPickItem->GetClass();
}

// Called every frame
void AWalls::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime); 
}

void AWalls::Interact(AActor* Target, bool bIsHeadElement, int AdditionalIndex)
{
	auto Snake = Cast<ASnakeBase>(Target);
	if (IsValid(Snake))
	{
		GetWorld()->GetGameInstance<USnake20GameInstance>()-> ResetLevel();
		this->Destroy();
		/*FVector NewLocation(50, 50, 0);
		FTransform NewTransform(NewLocation + GetActorTransform().GetLocation());
		ALevelPickItem* NewElement = GetWorld()->SpawnActor<ALevelPickItem>(LevelPickClass, NewTransform);
		Snake->Destroy();
		if (NewElement)
		{
			UE_LOG(LogTemp, Warning, TEXT("LvlPickItem spawn at: %f,%f,%f"), NewElement->GetActorLocation().X, NewElement->GetActorLocation().Y, NewElement->GetActorLocation().Z);
		}*/
	}
}
 