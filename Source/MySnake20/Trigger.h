// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Interactable.h"
#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "GameFramework/Actor.h"
#include "Trigger.generated.h"



UCLASS()
class MYSNAKE20_API ATrigger : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATrigger();
	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite)
	UStaticMeshComponent* MeshComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		AActor* TargetActor;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FVector TargetEndLocation;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float MoveSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool IsReversable;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int MinLenToRun;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int MaxLenToRun;

	FVector TargetStartLocation;
	bool isMovingTarget = false;

	ASnakeBase* MySnakeOverlap;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void MovingTarget();
	void BeginOverlap(UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor  ,
UPrimitiveComponent* OtherComp,
int32 OtherBodyIndex,
bool bFromSweep,
const FHitResult &SweepResult);

	virtual  void Interact(AActor* Target, bool bIsHeadElement, int AdditionalIndex) override;
};
