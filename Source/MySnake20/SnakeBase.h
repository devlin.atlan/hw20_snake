// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.generated.h"

class ASnakeElementBase;

UENUM()
enum class EMoveDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};



UCLASS()
class MYSNAKE20_API ASnakeBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeBase();

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeElementBase> SnakeElementClass;

	UPROPERTY()
	TArray<ASnakeElementBase*> SnakeElements;

	UPROPERTY(EditDefaultsOnly)
		float ElementSize;
	// "������", � ������� ����� ������
	UPROPERTY(EditDefaultsOnly)
		float StepWidth;
	//������� ������\�������� �����
	UPROPERTY()
		bool TickEvenCounter;
	//������ ������������	����������� �������� � ���������� ���. ����������� ����, ����� ����������� �������� ������ � ���� ���
	UPROPERTY()
		EMoveDirection PrevTickMoveDirection;
	UPROPERTY(EditDefaultsOnly)
		float Speed;
	UPROPERTY(EditDefaultsOnly)
	float DefaultZLocation=40.f;
	UPROPERTY()
		EMoveDirection CurrentMoveDirection;
///��� ����� ������� - ������ � ���������� ������� � ����� �������� ������.
	UPROPERTY(EditDefaultsOnly)
		TArray<FString> GameLevels;
	//TArray<ULevel*> GameLevels;
	UPROPERTY(EditDefaultsOnly)
		int CurrentGameLevelIndex;
	UPROPERTY(EditAnywhere)
		bool MovingCircle = false;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	UFUNCTION(BlueprintCallable)
		void AddSnakeElements(int ElementsCnt = 1, bool isStart = false);
		void AddSnakeElements(FVector* NewLocation, int ElementsCnt = 1, bool isStart = false);
	UFUNCTION(BlueprintCallable)
	void Move(bool isCircleMove=false);
	UFUNCTION()
		void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* OtherActor);
	UFUNCTION()
		void ChangeLevel(int Index = 0);	
	UFUNCTION()
		void FillPreparedElements();
};
