// Fill out your copyright notice in the Description page of Project Settings.
#include "Trigger.h"

// Sets default values
ATrigger::ATrigger()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
}

// Called when the game starts or when spawned
void ATrigger::BeginPlay()
{
	Super::BeginPlay();
	MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ATrigger::BeginOverlap);
}

// Called every frame
void ATrigger::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (isMovingTarget)
	{
		MovingTarget();
	}
}

void ATrigger::MovingTarget()
{
	FVector StepVector = (TargetEndLocation - TargetStartLocation)/MoveSpeed;
	FVector CurrentDeltaVector = TargetEndLocation - TargetActor->GetActorLocation();

	if (GetActorLocation() != TargetEndLocation)
	{
		if (CurrentDeltaVector.GetAbs().X<StepVector.GetAbs().X)
		{StepVector.X = CurrentDeltaVector.X;}
		if (CurrentDeltaVector.GetAbs().Y<StepVector.GetAbs().Y)
		{StepVector.Y = CurrentDeltaVector.Y;}
		if (CurrentDeltaVector.GetAbs().Z<StepVector.GetAbs().Z)
		{StepVector.Z = CurrentDeltaVector.Z;}
		TargetActor->AddActorWorldOffset(StepVector);
//		UE_LOG(LogTemp, Warning, TEXT("Moving-triggered!"));
	}
	else
	{
		isMovingTarget = false;
	}
}
void ATrigger::BeginOverlap(UPrimitiveComponent* OverlappedComponent,
        AActor* OtherActor
        ,
        UPrimitiveComponent* OtherComp,
        int32 OtherBodyIndex,
        bool bFromSweep,
        const FHitResult &SweepResult)
{
		UE_LOG(LogTemp, Warning, TEXT("Trigger in!"));
	if (Cast<ASnakeElementBase>(OtherActor) && MySnakeOverlap->SnakeElements[MySnakeOverlap->SnakeElements.Num() - 1] ==
		Cast<ASnakeElementBase>(OtherActor))
	{
		isMovingTarget = true;
		FVector temp = TargetEndLocation;
		TargetEndLocation = TargetStartLocation;
		TargetStartLocation = temp;
		SetActorHiddenInGame(false);	
	}
}


void ATrigger::Interact(AActor* Target, bool bIsHeadElement, int AdditionalIndex)
{
	if (!isMovingTarget && bIsHeadElement && MinLenToRun<=Cast<ASnakeBase>(Target)->SnakeElements.Num()&&Cast<ASnakeBase>(Target)->SnakeElements.Num()<=MaxLenToRun)
	{
		
		
		UE_LOG(LogTemp, Warning, TEXT("Triggered!"));

		TargetActor->SetActorHiddenInGame(false);	
		SetActorHiddenInGame(true);	
	//	AddActorWorldOffset(FVector(0, 0, -50));
		isMovingTarget = true;
		TargetStartLocation = TargetActor->GetActorLocation();

		  MySnakeOverlap  = Cast<ASnakeBase>(Target);
		/*if (IsValid(Snake))
		{
			Snake->AddSnakeElements();
			//�������� ������ ���� FOODS �� �����. ���� ���� ��������� - ������� �������.
			TArray<AActor*> Foods;
			UGameplayStatics::GetAllActorsOfClass(GetWorld(), AFood::StaticClass(), Foods);
			if (Foods.Num() == 1)
			{
				GetWorld()->GetGameInstance<USnake20GameInstance>()->ChangeLevel();
			}
			this->Destroy();
		}*/
	}
	if(AdditionalIndex ==Cast<ASnakeBase>(Target)->SnakeElements.Num()-1)
	{
	/*	SetActorHiddenInGame(false);
		TargetEndLocation = TargetStartLocation;
		TargetStartLocation = TargetActor->GetActorLocation();
		UE_LOG(LogTemp, Warning, TEXT("Trigger left!"));*/
	}
}

